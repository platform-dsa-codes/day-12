import java.util.*;
import java.lang.*;
import java.io.*;

class pair  {  
    long first, second;  
    public pair(long first, long second)  
    {  
        this.first = first;  
        this.second = second;  
    }  
}

class GFG {
	public static void main(String[] args) throws IOException
	{
	        BufferedReader br =
            new BufferedReader(new InputStreamReader(System.in));
        int t =
            Integer.parseInt(br.readLine().trim()); // Inputting the testcases
        while(t-->0)
        {
            long n = Long.parseLong(br.readLine().trim());
            long a[] = new long[(int)(n)];
            // long getAnswer[] = new long[(int)(n)];
            String inputLine[] = br.readLine().trim().split(" ");
            for (int i = 0; i < n; i++) {
                a[i] = Long.parseLong(inputLine[i]);
            }
            long k = Long.parseLong(br.readLine().trim());
            
            Solution obj = new Solution();
            pair ans = obj.indexes(a, k);
            if (ans.first == -1 && ans.second == -1)
                System.out.println(-1);
            else
                System.out.println(ans.first+" "+ans.second);
            
        }
	}
}


class Solution {
    
    public pair indexes(long v[], long x)
    {
        long first = findFirstOccurrence(v, x);
        long last = findLastOccurrence(v, x);
        
        return new pair(first, last);
    }
    
    private long findFirstOccurrence(long[] v, long x) {
        long low = 0;
        long high = v.length - 1;
        long firstOccurrence = -1;
        
        while (low <= high) {
            long mid = low + (high - low) / 2;
            
            if (v[(int)mid] == x) {
                firstOccurrence = mid;
                high = mid - 1; // Check left side
            } else if (v[(int)mid] < x) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }
        }
        
        return firstOccurrence;
    }
    
    private long findLastOccurrence(long[] v, long x) {
        long low = 0;
        long high = v.length - 1;
        long lastOccurrence = -1;
        
        while (low <= high) {
            long mid = low + (high - low) / 2;
            
            if (v[(int)mid] == x) {
                lastOccurrence = mid;
                low = mid + 1; // Check right side
            } else if (v[(int)mid] < x) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }
        }
        
        return lastOccurrence;
    }
}
