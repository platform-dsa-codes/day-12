import java.io.*;
import java.util.*;

class GFG {
    
	public static void main (String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int testcases = Integer.parseInt(br.readLine());
		
		while(testcases-- > 0){
		    int sizeOfArray = Integer.parseInt(br.readLine());
		    int arr [] = new int[sizeOfArray];
		    
		    String line = br.readLine();
		    String[] elements = line.trim().split("\\s+");
		    for(int i = 0;i<sizeOfArray;i++){
		        arr[i] =  Integer.parseInt(elements[i]);
		    }
		    
		    int sizeOfArray_M = Integer.parseInt(br.readLine());
		    int brr [] = new int[sizeOfArray_M];
		    
		    
		    line = br.readLine();
		    String[] ele = line.trim().split("\\s+");
		    for(int i = 0;i<sizeOfArray_M;i++){
		        brr[i] =  Integer.parseInt(ele[i]);
		    }
		    
		    
		    Solution obj = new Solution();
		    long res = obj.find_multiplication(arr, brr, sizeOfArray, sizeOfArray_M);
		    System.out.println(res);
		}
	}
}


class Solution{
    
    public static long find_multiplication (int arr[], int brr[], int n, int m) {
        int maxA = Integer.MIN_VALUE;
        for (int i = 0; i < n; i++) {
            maxA = Math.max(maxA, arr[i]);
        }
        
        int minB = Integer.MAX_VALUE;
        for (int i = 0; i < m; i++) {
            minB = Math.min(minB, brr[i]);
        }
        
        return (long) maxA * minB;
    }
}
